//
//  STKP2Constants.h
//  Stackpop 2
//
//  Created by Jacob Budin on 2/2/14.
//  Copyright (c) 2014 Jacob Budin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STKP2Constants : NSObject

extern NSString *STKP2OAuthEndpoint;
extern NSString *STKP2OAuthTokenEndpoint;
extern NSString *STKP2OAuthRedirectUri;
extern NSString *STKP2OAuthClientId;
extern NSString *STKP2OAuthClientSecret;
extern NSString *STKP2OAuthKey;
extern NSString *STKP2OAuthScope;
extern NSString *STKP2HelpUrl;

extern const float STKP2LoadInterval;

@end
