//
//  STKP2PreferencesDelegate.m
//  Stackpop 2
//
//  Created by Jacob Budin on 2/2/14.
//  Copyright (c) 2014 Jacob Budin. All rights reserved.
//

#import "STKP2PreferencesDelegate.h"

@implementation STKP2PreferencesDelegate

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if ([self accessToken]) {
        [self loadData];
    }
    else {
        [[self progressIndicator] setHidden:YES];
    }
}

- (void)windowDidLoad
{
    [[self preferencesWindow] makeKeyWindow];
    [NSApp activateIgnoringOtherApps:YES];
}


- (IBAction)launchHelp:(id)sender{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:STKP2HelpUrl]];
}

- (void)loadData{
    [self getSites];
    [self getUser];
}

- (IBAction)toggleAgent:(id)sender {
    NSURL *helperURL = [[[NSBundle mainBundle] bundleURL] URLByAppendingPathComponent:@"Contents/Library/LoginItems/StackpopAgent.app"];
    OSStatus status = LSRegisterURL((__bridge CFURLRef)helperURL, YES);
    if (status != noErr) {
        NSLog(@"Failed to LSRegisterURL '%@': %jd", helperURL, (intmax_t)status);
    }
    
    Boolean success = SMLoginItemSetEnabled(CFSTR("com.Budin.StackpopAgent"), YES);
    if (!success) {
        NSLog(@"Failed to start Helper");
    }
}

- (NSDictionary*)getPrimarySite{
    NSString *siteUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"com.Budin.Stackpop.primarySiteUrl"];
    
    for (NSDictionary* site in [self sites]) {
        if ([siteUrl isEqualToString:[site objectForKey:@"site_url"]]) {
            [self setPrimarySite:site];
            [[self sitesPopUpButton] selectItemWithTitle:[[site objectForKey:@"site_name"] gtm_stringByUnescapingFromHTML]];
            return site;
        }
    }
    
    if ([[self sites] count] > 0) {
        return [self autoSelectPrimarySite];
    }
    
    return nil;
}

- (IBAction)selectPrimarySite:(id)sender {
    NSString *primarySiteTitle = [sender titleOfSelectedItem];
    
    for (NSDictionary* site in [self sites]) {
        if ([[[site objectForKey:@"site_name"] gtm_stringByUnescapingFromHTML] isEqualToString:primarySiteTitle]) {
            [self setPrimarySite:site];
        }
    }
}

- (void)setPrimarySite: (NSDictionary*)site{
    [[NSUserDefaults standardUserDefaults] setObject:[site objectForKey:@"site_url"] forKey:@"com.Budin.Stackpop.primarySiteUrl"];
    [[NSUserDefaults standardUserDefaults] setObject:[[site objectForKey:@"site_name"] gtm_stringByUnescapingFromHTML] forKey:@"com.Budin.Stackpop.primarySiteName"];
}

- (NSDictionary*)autoSelectPrimarySite{
    for (NSDictionary* site in [self sites]) {
        [self setPrimarySite:site];
        return site;
    }
    
    return nil;
}


- (void)getSites{
    NSString *s = [NSString stringWithFormat:@"https://api.stackexchange.com/2.1/me/associated?access_token=%@&key=%@",
                   [[self accessToken] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                   [STKP2OAuthKey stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURL *url = [NSURL URLWithString:s];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    NSOperationQueue *q = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:req queue:q completionHandler:^(NSURLResponse *resp, NSData *d, NSError *err) {
        if (d) {
            NSError *requestError;
            NSMutableDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:d options:kNilOptions error:&requestError];
            NSArray *sites = [jsonResponse objectForKey:@"items"];
            [self setSites:sites];
        }
    }];
}

- (void)getUser{
    [[self progressIndicator] setHidden:NO];
    [[self progressIndicator] startAnimation:self];
    NSString *s = [NSString stringWithFormat:@"https://api.stackexchange.com/2.1/me?access_token=%@&key=%@&site=stackoverflow",
                   [[self accessToken] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                   [STKP2OAuthKey stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURL *url = [NSURL URLWithString:s];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    NSOperationQueue *q = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:req queue:q completionHandler:^(NSURLResponse *resp, NSData *d, NSError *err) {
        if (d) {
            NSError *requestError;
            NSMutableDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:d options:kNilOptions error:&requestError];
            NSArray *items = [jsonResponse objectForKey:@"items"];
            NSDictionary *user = [items objectAtIndex:0];
            [self setUser:user];
            [[self progressIndicator] stopAnimation:self];
            [[self progressIndicator] setHidden:YES];
        }
    }];
}

- (NSArray*)sites{
    return _sites;
}

- (void)setSites:(NSArray*)sites{
    _sites = sites;
    [self checkIfAuthorized];
}

- (BOOL)authorized{
    return _authorized;
}

- (void)checkIfAuthorized{
    if ([self user] && [self sites] && ([[self sites] count] > 0)) {
        [self setAuthorized:YES];
        return;
    }
    
    [self setAuthorized:NO];
}

- (void)setAuthorized:(BOOL)isAuthorized{
    if ([self user]) {
        NSString *displayName = [[self user] objectForKey:@"display_name"];
        NSString *profileImage = [[self user] objectForKey:@"profile_image"];
        
        if (displayName) {
            [[self userNameTextField] setStringValue:displayName];
        }
        else {
            [[self userNameTextField] setStringValue:@""];
        }
        
        if (profileImage && ([profileImage length] > 0)) {
            NSURL *profileImageUrl = [NSURL URLWithString:profileImage];
            NSImage *profileImageImage = [[NSImage alloc] initWithContentsOfURL:profileImageUrl];
            [[self userImageView] setImage:profileImageImage];
        }
    }
    
    if (isAuthorized) {
        [[self authorizeButton] setTitle:@"Deauthorize"];
        
        for (NSDictionary* site in [self sites]) {
            [[self sitesPopUpButton] addItemWithTitle:[[site objectForKey:@"site_name"] gtm_stringByUnescapingFromHTML]];
        }
        
        [[self reloadSitesButton] setEnabled:YES];
        [self getPrimarySite];
        [self startManager];
    }
    else {
        [[self userNameTextField] setStringValue:@""];
        [[self authorizeButton] setTitle:@"Authorize"];
        [[self sitesPopUpButton] removeAllItems];
        [[self userImageView] setImage:[NSImage imageNamed:NSImageNameUser]];
        [[self reloadSitesButton] setEnabled:NO];
    }
    
    [[self authorizeButton] sizeToFit];
    
    _authorized = isAuthorized;
}

- (void)startManager{
    /*
     NSString *agentPath = [NSString stringWithFormat:@"%@/%@", [[[self bundle] resourceURL] path], @"Stackpop.app"];
     [[NSWorkspace sharedWorkspace] launchApplication:agentPath];
     */
    
    //NSURL *helperURL = [NSURL URLWithString:[NSString stringWithFormat:@"file://%@/Contents/Library/LoginItems/%@", [[[self bundle] bundleURL] path], @"StackpopAgent.app"]];
    
    /*
    NSURL *helperURL = [[[NSBundle mainBundle] bundleURL] URLByAppendingPathComponent:@"Contents/Library/LoginItems/StackpopAgent.app"];
    OSStatus status = LSRegisterURL((__bridge CFURLRef)helperURL, YES);
    if (status != noErr) {
        NSLog(@"Failed to LSRegisterURL '%@': %jd", helperURL, (intmax_t)status);
    }
     */
    
    /*
     NSString *agentPath = [NSString stringWithFormat:@"%@/Contents/Library/LoginItems/%@", [[[self bundle] bundleURL] path], @"StackpopAgent.app"];
     [[NSWorkspace sharedWorkspace] launchApplication:agentPath];
     */
    
    /*
    Boolean success = SMLoginItemSetEnabled(CFSTR("com.Budin.Stackpop"), YES);
    if (!success) {
        NSLog(@"Failed to start Helper");
    }
     */
    
    /*
     BOOL yes = SMLoginItemSetEnabled((__bridge CFStringRef)@"com.Budin.Stackpop-Agent", YES);
     if(yes){
     NSLog(@"YES");
     }
     else{
     NSLog(@"NO");
     }
     */
    
    /*
     void *asyncLaunchRefCon = NULL;
     FSRef fileRef;
     CFArrayRef args = NULL;
     CFURLRef url;
     CFStringRef path;
     NSLog(@"%@", agentPath);
     path = CFStringCreateWithCString(NULL, [agentPath UTF8String], kCFStringEncodingUTF8);
     NSLog(@"%@", path);
     url = CFURLCreateWithFileSystemPath(NULL, path, kCFURLPOSIXPathStyle, false);
     NSLog(@"%@", url);
     CFURLGetFSRef(url, &fileRef);
     
     //NSLog(@"%@", [NSString stringWithFormat:@"%@%@", [[self bundle] resourceURL], @"Contents/Resources/Stackpop"]);
     //NSLog(@"%hhd", [[NSWorkspace sharedWorkspace] launchApplication:[NSString stringWithFormat:@"%@%@", [[self bundle] resourceURL], @"Contents/Resources/Stackpop"]]);
     LSApplicationParameters par = { 0, 0, &fileRef, asyncLaunchRefCon, 0, args, 0 };
     NSLog(@"%d", LSOpenApplication(&par, NULL));
     */
}

- (NSDictionary*)user{
    return _user;
}

- (void)setUser:(NSDictionary*)user{
    _user = user;
    [self checkIfAuthorized];
}

- (void)getAccessToken{
    NSURL* url = [NSURL URLWithString:STKP2OAuthTokenEndpoint];
    
    NSMutableURLRequest* req = [NSMutableURLRequest requestWithURL:url];
    [req setHTTPMethod:@"POST"];
    
    NSString *postString = [NSString stringWithFormat: @"client_id=%@&client_secret=%@&code=%@&redirect_uri=%@",
                            [STKP2OAuthClientId stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                            [STKP2OAuthClientSecret stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                            [[self accessCode] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                            [STKP2OAuthRedirectUri stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    [req setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSOperationQueue* q = [NSOperationQueue mainQueue];
    [NSURLConnection sendAsynchronousRequest:req queue:q completionHandler:^(NSURLResponse *resp, NSData *d, NSError *err) {
        if (d) {
            NSString *responseString = [[NSString alloc] initWithData:d encoding:NSUTF8StringEncoding];
            
            if ([responseString hasPrefix:@"access_token="]) {
                NSString *accessToken = [responseString substringFromIndex:13];
                [self setAccessToken:accessToken];
                [self loadData];
            }
        }
    }];
}

- (IBAction)reloadSites:(id)sender{
    [[self sitesPopUpButton] setEnabled:NO];
    [self getSites];
    [[self sitesPopUpButton] setEnabled:YES];
}

- (NSString*)accessToken{
    if (!_accessToken) {
        NSString* accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"com.Budin.Stackpop.accessToken"];
        
        if (accessToken && ([accessToken length] > 0)) {
            _accessToken = accessToken;
        }
    }
    
    return _accessToken;
}

- (void)setAccessToken:(NSString *)accessToken{
    [[NSUserDefaults standardUserDefaults] setObject:accessToken forKey:@"com.Budin.Stackpop.accessToken"];
    _accessToken = accessToken;
}

- (void)deauthorize{
    [self setUser:nil];
    [self setSites:[[NSArray alloc] init]];
    [self setAccessToken:@""];
}

- (IBAction)authorize:(id)sender {
    if ([self authorized]) {
        [self deauthorize];
        return;
    }
    
    // Generate OAuth URL
    NSString *queryString = @"?";
    NSDictionary *oauthQueryString = @{
                                       @"client_id": STKP2OAuthClientId,
                                       @"scope": STKP2OAuthScope,
                                       @"redirect_uri": STKP2OAuthRedirectUri,
                                       };
    
    NSArray *oauthQueryStringKeys = [oauthQueryString allKeys];
    NSUInteger oauthQueryStringKeyCount = [oauthQueryStringKeys count];
    
    for (int i = 0; i < oauthQueryStringKeyCount; i++) {
        NSString *newPathKey = [oauthQueryStringKeys objectAtIndex:i];
        NSString *newPathValue = [oauthQueryString objectForKey:newPathKey];
        NSString *newPathValueEncoded = [newPathValue stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *newPathComponent = [NSString stringWithFormat:@"%@=%@", newPathKey, newPathValueEncoded];
        
        queryString = [queryString stringByAppendingFormat:@"%@&", newPathComponent];
    }
    
    NSURL *oauthUrl = [NSURL URLWithString:[STKP2OAuthEndpoint stringByAppendingString:queryString]];
    
    // Load authorization window
    if ([[NSBundle mainBundle] loadNibNamed:@"Authorization" owner:self topLevelObjects:nil]) {
        [[[self authorizationWindow] contentView] setHidden:NO];
        [[[self authorizationWebView] mainFrame] loadRequest:[NSURLRequest requestWithURL:oauthUrl]];
    }
    
    // Check authorization window
    NSMethodSignature *timerSignature = [[self class] instanceMethodSignatureForSelector:@selector(checkAuthorization:)];
    NSInvocation *timerInvocation = [NSInvocation invocationWithMethodSignature:timerSignature];
    [timerInvocation setTarget:self];
    [timerInvocation setSelector:@selector(checkAuthorization:)];
    double timerTimeInterval = 0.25;
    
    NSTimer *authorizationTimer = [NSTimer scheduledTimerWithTimeInterval:timerTimeInterval invocation:timerInvocation repeats:YES];
    [self setAuthorizationTimer:authorizationTimer];
}

- (void)checkAuthorization:(id)sender {
    NSURL *currentUrl = [[[[[self authorizationWebView] mainFrame] dataSource] response] URL];
    
    if ([[currentUrl absoluteString] hasPrefix:STKP2OAuthRedirectUri]) {
        NSString *currentUrlQuery = [currentUrl query];
        
        if ([currentUrlQuery hasPrefix:@"code="]) {
            NSString *accessCode = [currentUrlQuery substringFromIndex:5];
            [self setAccessCode:accessCode];
            
            [[self authorizationWindow] close];
            
            if ([self authorizationTimer] != nil) {
                [[self authorizationTimer] invalidate];
                [self getAccessToken];
            }
        }
    }
}

- (IBAction)userName:(id)sender {
}
@end
